package com.pagoda.kitab;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;

@SuppressLint("NewApi")
public class CategoryActivity extends Activity {

	GridView gridview;
	String[] categories = { "IOM", "IOE", "CMAT", "IOST" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.category_layout);
		ActionBar ab = getActionBar();
		ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006600")));

		gridview = (GridView) findViewById(R.id.gridViewCategory);
		gridview.setVisibility(View.GONE);

		
		ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(),
				R.layout.grid_layout, R.id.categoryGridTitle, categories);
		gridview.setAdapter(adapter);
		gridview.postDelayed(new Runnable() {
			@Override
			public void run() {
				gridview.setVisibility(View.VISIBLE);
			}
		}, 1000);

		gridview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				Integer categoryIdInt = position;

				Intent i = new Intent(CategoryActivity.this,
						SetListActivity.class);
				i.putExtra("categoryId", categoryIdInt.toString());

				startActivity(i);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_left);

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case R.id.aboutMenu:
			AlertDialog alertDialog = showDialog();
			alertDialog.show();
			break;

		case R.id.feedbackMenu:
			Intent i = new Intent(CategoryActivity.this, FeedBackActivity.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_left);
			break;

		case R.id.historyMenu:
			Intent intent = new Intent(CategoryActivity.this,
					HistoryActivity.class);
			startActivity(intent);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_left);

		}
		return super.onOptionsItemSelected(item);
	}

	public AlertDialog showDialog() {
		AlertDialog alertDialogBuilder = new AlertDialog.Builder(
				CategoryActivity.this)

				.setTitle("About Us")
				.setMessage(getResources().getString(R.string.about_string))
				.setCancelable(false)

				.setPositiveButton("Close",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
							}
						})

				.create();

		return alertDialogBuilder;
	}

}
