package com.pagoda.kitab.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.pagoda.kitab.model.Questions;

public class DBHandler extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "Kitab";
	private static final int DATABASE_VERSION = 1;

	// Questions table name
	private static final String TABLE_QUESTIONS = "Questions";

	// Questions Table Columns names
	private static final String KEY_QUESTION_ID = "question_id";
	private static final String KEY_QUESTION_TITLE = "question_title";
	private static final String KEY_CATEGORY_ID = "category_id";
	private static final String KEY_SET_ID = "set_id";
	private static final String KEY_OPTION_1 = "option1";
	private static final String KEY_OPTION_2 = "option2";
	private static final String KEY_OPTION_3 = "option3";
	private static final String KEY_OPTION_4 = "option4";
	private static final String KEY_CORRECT_ANSWER = "correct_answer";
	private static final String KEY_IS_CLICKED="is_clicked";
	private static final String KEY_IS_CORRECT="is_correct";

	public DBHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public DBHandler(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) throws SQLiteException {
		// TODO Auto-generated method stub
		String CREATE_QUESTIONS_TABLE = "CREATE TABLE " + TABLE_QUESTIONS + "("
				+ KEY_QUESTION_ID + " INTEGER," + KEY_CATEGORY_ID + " INTEGER,"
				+ KEY_SET_ID + " INTEGER," + KEY_QUESTION_TITLE + " TEXT,"
				+ KEY_OPTION_1 + " TEXT," + KEY_OPTION_2 + " TEXT,"
				+ KEY_OPTION_3 + " TEXT," + KEY_OPTION_4 + " TEXT,"
				+ KEY_CORRECT_ANSWER + " TEXT" + ")";
		db.execSQL(CREATE_QUESTIONS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONS);

		// Create tables again
		onCreate(db);

	}

	// Adding new product
	public void addQuestions(Questions question) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_QUESTION_ID, question.getQuestion_id());
		values.put(KEY_CATEGORY_ID, question.getCategoryId());
		values.put(KEY_SET_ID, question.getSet_id());
		values.put(KEY_QUESTION_TITLE, question.getQuestion_title());
		values.put(KEY_OPTION_1, question.getOption_1());
		values.put(KEY_OPTION_2, question.getOption_2());
		values.put(KEY_OPTION_3, question.getOption_3());
		values.put(KEY_OPTION_4, question.getOption_4());
		values.put(KEY_CORRECT_ANSWER, question.getCorrect_answer());

		// Inserting Row
		db.insert(TABLE_QUESTIONS, null, values);
		db.close(); // Closing database connection
	}
	
	

	/*
	 * public int getFirstId(int category_id, int set_id) { int id = 0; String
	 * selectQuery = "SELECT  question_id FROM " + TABLE_QUESTIONS +
	 * " WHERE ( category_id=" + category_id + " AND set_id=" + set_id +
	 * " ) ORDER BY question_id ASC limit 1"; Log.d("SQL query", selectQuery);
	 * 
	 * SQLiteDatabase db = this.getWritableDatabase(); Cursor cursor =
	 * db.rawQuery(selectQuery, null);
	 * 
	 * // looping through all rows and adding to list if (cursor.moveToFirst())
	 * { do { id=Integer.parseInt(cursor.getString(0)); } while
	 * (cursor.moveToNext()); }
	 * 
	 * // return product list return id; }
	 */

	public int getLastId(int category_id, int set_id) {
		Integer questionId = 1;
		String selectQuery = "SELECT  question_id FROM " + TABLE_QUESTIONS
				+ " WHERE ( category_id=" + category_id + " AND set_id="
				+ set_id + " ) ORDER BY question_id DESC limit 1";
		Log.d("Lastid query", selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				questionId = Integer.parseInt(cursor.getString(0));
				// Log.d("lastid1",questionId.toString() );
			} while (cursor.moveToNext());
		}
		
		db.close();

		// return product list
		return questionId;
	}

	public int getInitialId(int category_id, int set_id) {
		Integer questionId = 1;
		String selectQuery = "SELECT  question_id FROM " + TABLE_QUESTIONS
				+ " WHERE ( category_id=" + category_id + " AND set_id="
				+ set_id + " ) ORDER BY question_id ASC limit 1";
		Log.d("Lastid query", selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				questionId = Integer.parseInt(cursor.getString(0));
				// Log.d("lastid1",questionId.toString() );
			} while (cursor.moveToNext());
		}
		
		db.close();

		// return product list
		return questionId;
	}

	public List<Questions> getAllQuestionsByCategorySet(int category_id,
			int set_id) {
		List<Questions> questionList = new ArrayList<Questions>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS
				+ " WHERE ( category_id=" + category_id + " AND set_id="
				+ set_id + " )";
		Log.d("SQL query", selectQuery);

		// String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS +
		// " WHERE category_id="+category_id+" ORDER BY RAND() LIMIT 1";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Questions question = new Questions();
				question.setQuestion_id(Integer.parseInt(cursor.getString(0)));
				question.setCategoryId(Integer.parseInt(cursor.getString(1)));
				question.setSet_id(Integer.parseInt(cursor.getString(2)));
				question.setQuestion_title(cursor.getString(3));
				question.setOption_1(cursor.getString(4));
				question.setOption_2(cursor.getString(5));
				question.setOption_3(cursor.getString(6));
				question.setOption_4(cursor.getString(7));
				question.setCorrect_answer(cursor.getString(8));
				// Adding product to list
				questionList.add(question);
			} while (cursor.moveToNext());
		}
		
		db.close();

		// return product list
		return questionList;
	}

	
	//get questions by category
	public List<Questions> getQuestionsByCategory(int category_id) {
		List<Questions> questionList = new ArrayList<Questions>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS
				+ " WHERE ( category_id=" + category_id + " )";
		Log.d("SQL query", selectQuery);

		// String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS +
		// " WHERE category_id="+category_id+" ORDER BY RAND() LIMIT 1";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Questions question = new Questions();
				question.setQuestion_id(Integer.parseInt(cursor.getString(0)));
				question.setCategoryId(Integer.parseInt(cursor.getString(1)));
				question.setSet_id(Integer.parseInt(cursor.getString(2)));
				question.setQuestion_title(cursor.getString(3));
				question.setOption_1(cursor.getString(4));
				question.setOption_2(cursor.getString(5));
				question.setOption_3(cursor.getString(6));
				question.setOption_4(cursor.getString(7));
				question.setCorrect_answer(cursor.getString(8));
				// Adding product to list
				questionList.add(question);
			} while (cursor.moveToNext());
		}
		
		db.close();

		// return product list
		return questionList;
	}

	public List<Questions> getQuestionsByQuestionId(int category_id,
			int set_id, int question_id) {
		List<Questions> questionList = new ArrayList<Questions>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS
				+ " WHERE ( category_id=" + category_id + " AND set_id="
				+ set_id + " AND question_id=" + question_id + " )";
		Log.d("SQL query", selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Questions question = new Questions();
				question.setQuestion_id(Integer.parseInt(cursor.getString(0)));
				question.setCategoryId(Integer.parseInt(cursor.getString(1)));
				question.setSet_id(Integer.parseInt(cursor.getString(2)));
				question.setQuestion_title(cursor.getString(3));
				question.setOption_1(cursor.getString(4));
				question.setOption_2(cursor.getString(5));
				question.setOption_3(cursor.getString(6));
				question.setOption_4(cursor.getString(7));
				question.setCorrect_answer(cursor.getString(8));
				// Adding product to list
				questionList.add(question);
			} while (cursor.moveToNext());
		}

		db.close();
		// return product list
		return questionList;
	}

	// Getting All Questions
	public List<Questions> getAllQuestionsById(int question_id, int set_id) {
		List<Questions> questionList = new ArrayList<Questions>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS
				+ " WHERE (set_id=" + set_id + " AND question_id="
				+ question_id + ") ORDER BY RANDOM() LIMIT 1";
		Log.d("Query", selectQuery);
		// String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS +
		// " WHERE category_id="+category_id+" ORDER BY RAND() LIMIT 1";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Questions question = new Questions();
				question.setQuestion_id(Integer.parseInt(cursor.getString(0)));
				question.setCategoryId(Integer.parseInt(cursor.getString(1)));
				question.setSet_id(Integer.parseInt(cursor.getString(2)));
				question.setQuestion_title(cursor.getString(3));
				question.setOption_1(cursor.getString(4));
				question.setOption_2(cursor.getString(5));
				question.setOption_3(cursor.getString(6));
				question.setOption_4(cursor.getString(7));
				question.setCorrect_answer(cursor.getString(8));
				// Adding product to list
				questionList.add(question);
			} while (cursor.moveToNext());
		}

		db.close();
		// return product list
		return questionList;
	}

	// Getting all question
	public List<Questions> getAllQuestions() {
		List<Questions> questionList = new ArrayList<Questions>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS
				+ " ORDER BY RANDOM() LIMIT 1";

		// String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS +
		// " WHERE category_id="+category_id+" ORDER BY RAND() LIMIT 1";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Questions question = new Questions();
				question.setQuestion_id(Integer.parseInt(cursor.getString(0)));
				question.setCategoryId(Integer.parseInt(cursor.getString(1)));
				question.setSet_id(Integer.parseInt(cursor.getString(2)));
				question.setQuestion_title(cursor.getString(3));
				question.setOption_1(cursor.getString(4));
				question.setOption_2(cursor.getString(5));
				question.setOption_3(cursor.getString(6));
				question.setOption_4(cursor.getString(7));
				question.setCorrect_answer(cursor.getString(8));
				// Adding product to list
				questionList.add(question);
			} while (cursor.moveToNext());
		}
		
		db.close();

		// return product list
		return questionList;
	}

	// Updating single product
	/*
	 * public int updateProduct(int id, String name, String price) {
	 * SQLiteDatabase db = this.getWritableDatabase();
	 * 
	 * ContentValues values = new ContentValues();
	 * values.put(KEY_QUESTION_TITLE, name); values.put(KEY_OPTION_1, price);
	 * 
	 * // updating row return db.update(TABLE_Questions, values, KEY_ID +
	 * " = ?", new String[] { String.valueOf(id) }); }
	 */

	// Deleting single product
	public void deleteQuestionById(long id) {
		SQLiteDatabase db = this.getWritableDatabase();
		String deleteQuery = "DELETE FROM " + TABLE_QUESTIONS + " where id="
				+ id;
		Log.d("query", deleteQuery);
		db.execSQL(deleteQuery);
		db.close();
	}

	public void deleteQuestion() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_QUESTIONS, null, null);
		db.close();
	}
}
