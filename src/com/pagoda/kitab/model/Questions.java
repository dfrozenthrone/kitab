package com.pagoda.kitab.model;

import java.io.Serializable;

public class Questions implements Serializable{
	
	private int questionId;
	private String questionTitle;
	private int setId;
	private int categoryId;
	private String option_1;
	private String option_2;
	private String option_3;
	private String option_4;
	private String correctAnswer;
	
	
	public Questions() {
		super();
	}
	
	public Questions(int questionId,int categoryId,int setId, String questionTitle, String option_1,
			String option_2, String option_3, String option_4,
			String correctAnswer) {
		super();
		this.questionId=questionId;
		this.categoryId=categoryId;
		this.setId=setId;
		this.questionTitle = questionTitle;
		this.option_1 = option_1;
		this.option_2 = option_2;
		this.option_3 = option_3;
		this.option_4 = option_4;
		this.correctAnswer = correctAnswer;
	}

	public int getQuestion_id() {
		return questionId;
	}
	
	public void setQuestion_id(int questionId) {
		this.questionId = questionId;
	}
	
	public int getSet_id() {
		return setId;
	}

	public void setSet_id(int setId) {
		this.setId = setId;
	}

	public String getQuestion_title() {
		return questionTitle;
	}
	
	public void setQuestion_title(String questionTitle) {
		this.questionTitle = questionTitle;
	}
	
	public String getOption_1() {
		return option_1;
	}
	
	public void setOption_1(String option_1) {
		this.option_1 = option_1;
	}
	
	public String getOption_2() {
		return option_2;
	}
	
	public void setOption_2(String option_2) {
		this.option_2 = option_2;
	}
	
	public String getOption_3() {
		return option_3;
	}
	
	public void setOption_3(String option_3) {
		this.option_3 = option_3;
	}
	
	public String getOption_4() {
		return option_4;
	}
	
	public void setOption_4(String option_4) {
		this.option_4 = option_4;
	}
	
	public String getCorrect_answer() {
		return correctAnswer;
	}
	
	public void setCorrect_answer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	
}
