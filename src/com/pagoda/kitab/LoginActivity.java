package com.pagoda.kitab;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;

public class LoginActivity extends Activity {

	Facebook facebook;
	ImageView button;
	String FILENAME = "AndroidSSO_data";
	private SharedPreferences mPrefs;
	TextView welcome;
	AsyncFacebookRunner asyncRunner;
	JSONObject obj = null;
	Button fbBtn, postBtn;
	ProgressDialog pDialog;
	String name, email;
	String id, full_name, first_name;
	Integer success;
	ProgressDialog mProgress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_layout);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		String APP_ID = getString(R.string.facebook_app_id);
		facebook = new Facebook(APP_ID);
		asyncRunner = new AsyncFacebookRunner(facebook);

		mPrefs = getPreferences(MODE_PRIVATE);
		/*
		 * String access_token = mPrefs.getString("access_token", null); long
		 * expires = mPrefs.getLong("access_expires", 0);
		 * 
		 * if(access_token!=null) { facebook.setAccessToken(access_token); }
		 * if(expires!=0) { facebook.setAccessExpires(expires); }
		 */
		fbBtn = (Button) findViewById(R.id.fbBtnLogin);
		welcome = (TextView) findViewById(R.id.welcomeTxt);
		postBtn = (Button) findViewById(R.id.postBtn);

		postBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				postToFacebook("Tthis is ttesest");

			}
		});

		/*
		 * fbBtn.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View arg0) { // TODO Auto-generated
		 * method stub
		 * 
		 * CheckFacebookLogin.execute();
		 * 
		 * } });
		 */

		fbBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				facebook.authorize(LoginActivity.this,
						new String[] { "email" }, new DialogListener() {

							@Override
							public void onFacebookError(FacebookError e) {
								// TODO Auto-generated method stub
								Toast.makeText(getApplicationContext(),
										"Facebook Error. Please try again.",
										Toast.LENGTH_SHORT).show();
							}

							@Override
							public void onError(DialogError e) {
								// TODO Auto-generated method stub
								Toast.makeText(getApplicationContext(),
										"Facebook Error. Please try again.",
										Toast.LENGTH_SHORT).show();
							}

							@Override
							public void onComplete(Bundle values) {
								// TODO Auto-generated method stub

								/*
								 * Editor edit =mPrefs.edit();
								 * edit.putString("access_token",
								 * facebook.getAccessToken());
								 * edit.putLong("expires",
								 * facebook.getAccessExpires()); edit.commit();
								 * updateInfo();
								 */

								try {
									String jsonUser = facebook.request("me");
									obj = Util.parseJson(jsonUser);
									String firstName = obj.optString("name");
									Log.d("facebookk name", firstName);
									welcome.setText("welcome"+firstName);
									new CheckFacebookLogin().execute();

									/*
									 * // Editor editor = sp.edit(); //
									 * editor.putString("flag", "1"); //
									 * editor.putString("user_id", id); //
									 * editor.putString("first_name", //
									 * first_name); //
									 * editor.putString("last_name", last_name);
									 * // editor.putString("email", email); //
									 * editor.commit();
									 * 
									 * // login(); // // user.setText(""); //
									 * pass.setText("");
									 */

								} catch (Exception ex) {
									ex.printStackTrace();
								} catch (FacebookError e) { // TODO
															// Auto-generated
															// catch block
									e.printStackTrace();
								}

							}

							@Override
							public void onCancel() {
								// TODO Auto-generated method stub
								// Toast.makeText(getActivity(), "Cancelled.",
								// Toast.LENGTH_SHORT).show();
							}
						});
			}
		});

	}

	/*
	 * public void getProfileInformation() {    asyncRunner.request("me", new
	 * RequestListener() {
	 * 
	 * @Override public void onMalformedURLException(MalformedURLException e,
	 * Object state) { // TODO Auto-generated method stub
	 * 
	 * }
	 * 
	 * @Override public void onIOException(IOException e, Object state) { //
	 * TODO Auto-generated method stub
	 * 
	 * }
	 * 
	 * @Override public void onFileNotFoundException(FileNotFoundException e,
	 * Object state) { // TODO Auto-generated method stub
	 * 
	 * }
	 * 
	 * @Override public void onFacebookError(FacebookError e, Object state) { //
	 * TODO Auto-generated method stub
	 * 
	 * }
	 * 
	 * @Override public void onComplete(String response, Object state) { // TODO
	 * Auto-generated method stub
	 * 
	 * Log.d("Profile", response); String json = response; try { JSONObject
	 * profile = new JSONObject(json); // getting name of the user name =
	 * profile.getString("name"); // getting email of the user email =
	 * profile.getString("email");
	 * 
	 * runOnUiThread(new Runnable() {
	 * 
	 * @Override public void run() { Toast.makeText(getApplicationContext(),
	 * "Name: " + name + "\nEmail: " + email, Toast.LENGTH_LONG).show(); }
	 * 
	 * });
	 * 
	 * } catch (JSONException e) { e.printStackTrace(); }
	 * 
	 * } });          }
	 */

	/*
	 * public void loginToFacebook() { mPrefs = getPreferences(MODE_PRIVATE);
	 * String access_token = mPrefs.getString("access_token", null); long
	 * expires = mPrefs.getLong("access_expires", 0);
	 * 
	 * if (access_token != null) { facebook.setAccessToken(access_token); }
	 * 
	 * if (expires != 0) { facebook.setAccessExpires(expires); }
	 * 
	 * if (!facebook.isSessionValid()) { facebook.authorize(this, new String[] {
	 * "email", "publish_stream" }, new DialogListener() {
	 * 
	 * @Override public void onCancel() { // Function to handle cancel event }
	 * 
	 * @Override public void onComplete(Bundle values) { // Function to handle
	 * complete event // Edit Preferences and update facebook acess_token
	 * SharedPreferences.Editor editor = mPrefs.edit();
	 * editor.putString("access_token", facebook.getAccessToken());
	 * editor.putLong("access_expires", facebook.getAccessExpires());
	 * editor.commit(); }
	 * 
	 * @Override public void onError(DialogError error) { // Function to handle
	 * error
	 * 
	 * }
	 * 
	 * @Override public void onFacebookError(FacebookError fberror) { //
	 * Function to handle Facebook errors
	 * 
	 * }
	 * 
	 * }); } }
	 */

	/*
	 * public void updateInfo() { if (facebook.isSessionValid()) { try { String
	 * jsonUser = facebook.request("me"); obj = Util.parseJson(jsonUser);
	 * 
	 * Log.d("Facebook", obj.toString()); id = obj.optString("id"); full_name =
	 * obj.optString("name"); first_name = obj.optString("first_name");
	 * Log.d("firstname", first_name); String last_name =
	 * obj.optString("last_name"); String gender = obj.optString("gender");
	 * String email = obj.optString("email");
	 * 
	 * welcome.setText("Welcome" + first_name); } catch (JSONException json) {
	 * json.printStackTrace(); } catch (IOException e) { // TODO Auto-generated
	 * catch block e.printStackTrace(); } catch (FacebookError e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); }
	 * 
	 * } else { Toast.makeText(getApplicationContext(), "Plz logi",
	 * 3000).show(); } }
	 */

	private void postToFacebook(String review) {
		mProgress.setMessage("Posting ...");
		mProgress.show();

		AsyncFacebookRunner mAsyncFbRunner = new AsyncFacebookRunner(facebook);

		Bundle params = new Bundle();

		params.putString("message", review);
		params.putString("name", "Dexter");
		params.putString("caption", "londatiga.net");
		params.putString("link", "http://www.londatiga.net");
		params.putString(
				"description",
				"Dexter, seven years old dachshund who loves to catch cats, eat carrot and krupuk");
		params.putString("picture", "http://twitpic.com/show/thumb/6hqd44");

		mAsyncFbRunner.request("me/feed", params, "POST	",
				new WallPostListener(), true);
	}

	private final class WallPostListener implements RequestListener {

		@Override
		public void onComplete(String response, Object state) {
			// TODO Auto-generated method stub

			mProgress.cancel();

			Toast.makeText(LoginActivity.this, "Posted to Facebook",
					Toast.LENGTH_SHORT).show();

		}

		@Override
		public void onIOException(IOException e, Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {
			// TODO Auto-generated method stub

		}
	}

	class CheckFacebookLogin extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getApplicationContext());
			pDialog.setMessage("Checking..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		protected String doInBackground(String... args) {

			runOnUiThread(new Runnable() {
				public void run() {

					Log.d("Facebook", obj.toString());
					id = obj.optString("id");
					full_name = obj.optString("name");
					first_name = obj.optString("first_name");
					Log.d("firstname", first_name);
					String last_name = obj.optString("last_name");
					String gender = obj.optString("gender");
					String email = obj.optString("email");

					/*
					 * List<NameValuePair> params = new
					 * ArrayList<NameValuePair>(); params.add(new
					 * BasicNameValuePair("fb_id", id)); params.add(new
					 * BasicNameValuePair("first_name", first_name));
					 * params.add(new BasicNameValuePair("last_name",
					 * last_name)); params.add(new BasicNameValuePair("gender",
					 * gender)); params.add(new BasicNameValuePair("email",
					 * email));
					 * 
					 * JSONObject json = jsonParser.makeHttpRequest(AppConstants
					 * .url_check_login, "POST", params);
					 * 
					 * Log.d("FACEBOOK INFO", json.toString());
					 * 
					 * success = json.getInt(AppConstants.TAG_SUCCESS);
					 * if(success == 1) { JSONArray empObj =
					 * json.getJSONArray(AppConstants.TAG_USERS); JSONObject emp
					 * = empObj.getJSONObject(0);
					 * 
					 * user_id = emp.getString(AppConstants.TAG_USER_ID); name =
					 * emp.getString(AppConstants.TAG_NAME); }
					 */

				}
			});

			return null;
		}

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			// if (success == 1) {
			// Toast.makeText(getActivity(), "success=1",
			// Toast.LENGTH_LONG).show();
			/*
			 * Editor editor = sp.edit(); editor.putString("flag", "1");
			 * editor.putString("user_id", user_id); editor.putString("name",
			 * name); editor.commit();
			 */

			welcome.setText("Welcome, " + full_name + ".");

			/*
			 * } else if (success == 2) { // }
			 */
		}

	}

	public void postToWall() {
		// post on user's wall.
		facebook.dialog(this, "feed", new DialogListener() {

			@Override
			public void onFacebookError(FacebookError e) {
			}

			@Override
			public void onError(DialogError e) {
			}

			@Override
			public void onComplete(Bundle values) {
			}

			@Override
			public void onCancel() {
			}
		});

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);
	}

}
