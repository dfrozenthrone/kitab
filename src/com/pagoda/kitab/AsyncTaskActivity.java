package com.pagoda.kitab;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.pagoda.kitab.database.DBHandler;
import com.pagoda.kitab.model.Questions;
import com.pagoda.kitab.utils.JSONParser;

public class AsyncTaskActivity extends AsyncTask<String, String, String> {

	ListView setList;
	ArrayAdapter adapter;
	Context context;
	List<Questions> questions;
	DBHandler db = new DBHandler(context);

	String categoryIdIntent, setIdIntent;
	ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();
	JSONArray products = null;
	public static String url_all_products;

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(String... arg0) {
		// TODO Auto-generated method stub

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		// getting JSON string from URL
		JSONObject json = jParser.makeHttpRequest(url_all_products, "GET",
				params);

		// Check your log cat for JSON reponse
		Log.d("All Products: ", json.toString());

		try {
			// Checking for SUCCESS TAG
			int success = json.getInt(AppConstants.TAG_SUCCESS);

			if (success == 1) {
				// products found
				// Getting Array of Products
				products = json.getJSONArray(AppConstants.TAG_PRODUCTS);

				// looping through All Products
				for (int i = 0; i < products.length(); i++) {
					JSONObject c = products.getJSONObject(i);

					// Storing each json item in variable
					String questionId = c
							.getString(AppConstants.TAG_QUESTION_ID);
					String questionTitle = c
							.getString(AppConstants.TAG_QUESTION_TITLE);
					String setId = c.getString(AppConstants.TAG_SET_ID);
					String option1 = c.getString(AppConstants.TAG_OPTION1);
					String option2 = c.getString(AppConstants.TAG_OPTION2);
					String option3 = c.getString(AppConstants.TAG_OPTION3);
					String option4 = c.getString(AppConstants.TAG_OPTION4);
					String correctAnswer = c
							.getString(AppConstants.TAG_CORRECT_ANSWER);
					String categoryId = c
							.getString(AppConstants.TAG_CATEGORY_ID);

					// creating new HashMap
					/*
					 * HashMap<String, String> map = new HashMap<String,
					 * String>();
					 * 
					 * // adding each child node to HashMap key => value
					 * map.put(TAG_PID, id); map.put(TAG_IMAGE, image);
					 * 
					 * // adding HashList to ArrayList flowerList.add(map);
					 */

					// db.addFlower(new Flower("ids", id, "null", "null",
					// "null", "null", image, "null", "null", "null",
					// "null", "null"));

					Log.d("question id", questionId);
					Log.d("question id", questionTitle);
					Log.d("question id", setId);
					Log.d("question id", option1);
					Log.d("question id", option2);

					boolean insertion = false;
					try {
						db.addQuestions(new Questions(Integer
								.parseInt(questionId), Integer
								.parseInt(categoryId), Integer.parseInt(setId),
								questionTitle, option1, option2, option3,
								option4, correctAnswer));
						insertion = true;

						if (insertion) {
							Log.e("Success", "Inserted");
						}

					} catch (SQLiteException sqlite) {
						System.out.println(sqlite.getMessage());
					}
				}
			} else {

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		pDialog.dismiss();

		setList.postDelayed(new Runnable() {
			@Override
			public void run() {
				// showAll(); // or View.INVISIBLE as Jason Leung wrote
			}
		}, 1000);
	}

}
