package com.pagoda.kitab;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pagoda.kitab.database.DBHandler;
import com.pagoda.kitab.model.Questions;
import com.pagoda.kitab.utils.JSONParser;

public class SetListActivity extends Activity {

	ListView setList;
	ArrayAdapter adapter;
	TextView instructionTxt;
	String[] sets = { "Set 1", "Set 2", "Set 3", "Set 4", "Set 5", "Set 6",
			"Set 7", "Set 8", "Set 9", "Set 10" };
	ImageView downloadImageView;
	DBHandler db = new DBHandler(this);
	List<Questions> questions;

	String categoryIdIntent, setIdIntent;
	ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();
	JSONArray products = null;
	public static String url_all_products;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_set_layout);
		ActionBar ab = getActionBar();
		ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006600")));
		ab.setHomeButtonEnabled(true);

		categoryIdIntent = getIntent().getStringExtra("categoryId");
		setList = (ListView) findViewById(R.id.setListView);
		downloadImageView = (ImageView) findViewById(R.id.downloadImageView);
		instructionTxt = (TextView) findViewById(R.id.downloadText);
		downloadImageView.setVisibility(View.GONE);
		adapter = new ArrayAdapter(getApplicationContext(),
				R.layout.list_set_item, R.id.setId, sets);
		setList.setAdapter(adapter);

		categoryIdIntent = getIntent().getStringExtra("categoryId");

		if (Integer.parseInt(categoryIdIntent) == 0) {
			url_all_products = "http://api.manishm.com.np/kitab/index.php/question/json_data/1";
			ab.setSubtitle("IOM");
		} else if (Integer.parseInt(categoryIdIntent) == 1) {
			url_all_products = "http://api.manishm.com.np/kitab/index.php/question/json_data/2";

			ab.setSubtitle("IOE");
		} else if (Integer.parseInt(categoryIdIntent) == 2) {
			url_all_products = "http://api.manishm.com.np/kitab/index.php/question/json_data/3";
			ab.setSubtitle("CMAT");
		} else {
			url_all_products = "http://api.manishm.com.np/kitab/index.php/question/json_data/4";
			ab.setSubtitle("IOST");
		}

		try {
			questions = db.getQuestionsByCategory(Integer
					.parseInt(categoryIdIntent) + 1);

			Integer lengthQuestions = questions.size();
			Log.d("number of question", lengthQuestions.toString());
		} catch (SQLiteException e) {
			Log.d("Sqlite", e.getMessage());
		}

		if (questions.isEmpty()) {
			hideAll();
			downloadImageView.setVisibility(View.VISIBLE);
			instructionTxt.setVisibility(View.VISIBLE);

			downloadImageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					if (haveNetworkConnection()) {
						new LoadAllProducts().execute();
					} else {
						Toast.makeText(getApplicationContext(),
								"No Internet Connection", 3000).show();
					}
				}
			});

		}

		setList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				List<Questions> questionsBySet = db
						.getAllQuestionsByCategorySet(
								Integer.parseInt(categoryIdIntent) + 1,
								position + 1);

				if (questionsBySet.isEmpty()) {
					Toast.makeText(getApplicationContext(),
							"No Questions Available in this set", 3000).show();
				} else {

					Integer setIdInt = position;
					Intent i = new Intent(SetListActivity.this,
							MainActivity.class);
					// i.putExtra("Category_id", category_id);
					i.putExtra("setId", setIdInt.toString());
					i.putExtra("categoryId", categoryIdIntent);
					startActivity(i);
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_left);

				}

			}
		});

	}

	private boolean haveNetworkConnection() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		return super.onOptionsItemSelected(item);
	}

	public void hideAll() {
		setList.setVisibility(View.GONE);
		instructionTxt.setVisibility(View.GONE);
		downloadImageView.setVisibility(View.GONE);
	}

	public void showAll() {
		setList.setVisibility(View.VISIBLE);
	}

	class LoadAllProducts extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(SetListActivity.this);
			pDialog.setMessage("Downloading questions....");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(url_all_products, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt(AppConstants.TAG_SUCCESS);

				if (success == 1) {
					// products found
					// Getting Array of Products
					products = json.getJSONArray(AppConstants.TAG_PRODUCTS);

					// looping through All Products
					for (int i = 0; i < products.length(); i++) {
						JSONObject c = products.getJSONObject(i);

						// Storing each json item in variable
						String questionId = c
								.getString(AppConstants.TAG_QUESTION_ID);
						String questionTitle = c
								.getString(AppConstants.TAG_QUESTION_TITLE);
						String setId = c.getString(AppConstants.TAG_SET_ID);
						String option1 = c.getString(AppConstants.TAG_OPTION1);
						String option2 = c.getString(AppConstants.TAG_OPTION2);
						String option3 = c.getString(AppConstants.TAG_OPTION3);
						String option4 = c.getString(AppConstants.TAG_OPTION4);
						String correctAnswer = c
								.getString(AppConstants.TAG_CORRECT_ANSWER);
						String categoryId = c
								.getString(AppConstants.TAG_CATEGORY_ID);

						// creating new HashMap
						/*
						 * HashMap<String, String> map = new HashMap<String,
						 * String>();
						 * 
						 * // adding each child node to HashMap key => value
						 * map.put(TAG_PID, id); map.put(TAG_IMAGE, image);
						 * 
						 * // adding HashList to ArrayList flowerList.add(map);
						 */

						// db.addFlower(new Flower("ids", id, "null", "null",
						// "null", "null", image, "null", "null", "null",
						// "null", "null"));

						Log.d("question id", questionId);
						Log.d("question id", questionTitle);
						Log.d("question id", setId);
						Log.d("question id", option1);
						Log.d("question id", option2);

						boolean insertion = false;
						try {
							db.addQuestions(new Questions(Integer
									.parseInt(questionId), Integer
									.parseInt(categoryId), Integer
									.parseInt(setId), questionTitle, option1,
									option2, option3, option4, correctAnswer));
							insertion = true;

							if (insertion) {
								Log.e("Success", "Inserted");
							}

						} catch (SQLiteException sqlite) {
							System.out.println(sqlite.getMessage());
						}
					}
				} else {

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			hideAll();
			setList.postDelayed(new Runnable() {
				@Override
				public void run() {
					showAll(); // or View.INVISIBLE as Jason Leung wrote
				}
			}, 1000);

			/*
			 * // updating UI from Background Thread runOnUiThread(new
			 * Runnable() {
			 * 
			 * public void run() {
			 *//**
			 * Updating parsed JSON data into ListView
			 * */
			/*
			 * customAdapter = new BuyAdapter(getApplicationContext(),
			 * R.layout.grid_layout, flowerList);
			 * gridView.setAdapter(customAdapter);
			 * 
			 * } });
			 * 
			 * }
			 */
		}
	}

}
