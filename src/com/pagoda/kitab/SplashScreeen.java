package com.pagoda.kitab;

import android.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashScreeen extends Activity implements AnimationListener {

	// int SPLASH_TIME_OUT = 2000;
	// Animation
	Animation animSideDown, animFadein;

	int SPLASH_TIME_OUT = 2000;
	TextView titleTxt;
	ImageView iconImg;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(com.pagoda.kitab.R.layout.splash_layout);
		android.app.ActionBar ab = getActionBar();
		ab.hide();

		titleTxt = (TextView) findViewById(com.pagoda.kitab.R.id.splashTxt);
		iconImg = (ImageView) findViewById(com.pagoda.kitab.R.id.icImg);
		
		//change font
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/Roboto-Thin.ttf");
		titleTxt.setTypeface(tf);
		// load the animation
		animSideDown = AnimationUtils.loadAnimation(getApplicationContext(),
				com.pagoda.kitab.R.anim.slide_down);
		animFadein = AnimationUtils.loadAnimation(getApplicationContext(),
				com.pagoda.kitab.R.anim.fade_in);
		
		// set animation listener
		animSideDown.setAnimationListener(this);

		iconImg.setVisibility(View.VISIBLE);
		iconImg.startAnimation(animSideDown);
		
		
		animFadein.setAnimationListener(this);
		
		titleTxt.setVisibility(View.VISIBLE);
		titleTxt.startAnimation(animFadein);

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				Intent i = new Intent(SplashScreeen.this,
						CategoryActivity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_left,
						com.pagoda.kitab.R.anim.slide_out_left);

				finish();
			}
		}, SPLASH_TIME_OUT);

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub
		// check for zoom in animation
				if (animation == animSideDown) {			
				}

				if (animation == animFadein) {

				}
	}

	@Override
	public void onAnimationRepeat(Animation arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationStart(Animation arg0) {
		// TODO Auto-generated method stub
		
	}

}
