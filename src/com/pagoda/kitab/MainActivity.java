package com.pagoda.kitab;

import java.util.List;
import java.util.Random;

import net.frederico.showtipsview.ShowTipsBuilder;
import net.frederico.showtipsview.ShowTipsView;

import org.json.JSONArray;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pagoda.kitab.database.DBHandler;
import com.pagoda.kitab.model.Questions;
import com.pagoda.kitab.utils.JSONParser;

public class MainActivity extends Activity {

	TextView question, answer1, answer2, answer3, answer4, scoreTxt,
			numberHeader, categoryHeader, setHeader, noQuestionText, scoreInt,
			scoreTextInt, skipTxt;
	Button checkBtn;
	private static long back_pressed;
	ImageButton nextBtn, previousBtn;
	LinearLayout headerLayout;
	RelativeLayout scoreLayout, mainActivityLayout;

	private Integer id, initialId, lastId, score = 0, questionNumber = 1,
			lengthQuestions, tryScore = 0, correctScore = 0;
	private String questionTitle, option1, option2, option3, option4,
			right_answer;
	ImageView downloadImageView;
	DBHandler db = new DBHandler(this);
	List<Questions> questions;
	Questions eachQuestions;
	String categoryIdIntent, setIdIntent;
	ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();
	JSONArray products = null;
	public static String url_all_products;
	private static int TIME_QUESTION = 900;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ActionBar ab = getActionBar();

		Log.d("test", "asdfads");

		ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006600")));
		//ab.hide();

		categoryIdIntent = getIntent().getStringExtra("categoryId");
		setIdIntent = getIntent().getStringExtra("setId");

		intialize(); // initializes the buttons

		setHeader.setText("Set " + (Integer.parseInt(setIdIntent) + 1));

		if (Integer.parseInt(categoryIdIntent) == 0) {
			categoryHeader.setText("IOE");
		} else if (Integer.parseInt(categoryIdIntent) == 1) {
			categoryHeader.setText("IOM");
		} else if (Integer.parseInt(categoryIdIntent) == 2) {
			categoryHeader.setText("CMAT");
		} else {
			categoryHeader.setText("IOST");
		}

		try {

			lastId = db.getLastId((Integer.parseInt(categoryIdIntent) + 1),
					(Integer.parseInt(setIdIntent) + 1));
			Log.d("Last question id", lastId.toString());

			Integer Initial;
			initialId = db.getInitialId(
					(Integer.parseInt(categoryIdIntent) + 1),
					(Integer.parseInt(setIdIntent) + 1));
			questions = db.getQuestionsByQuestionId(
					(Integer.parseInt(categoryIdIntent) + 1),
					(Integer.parseInt(setIdIntent) + 1), initialId);
			List<Questions> questionsBySet = db.getAllQuestionsByCategorySet(
					(Integer.parseInt(categoryIdIntent) + 1),
					(Integer.parseInt(setIdIntent) + 1));
			lengthQuestions = questionsBySet.size();
			Log.d("number of question", lengthQuestions.toString());

			Log.d("Initial question id", initialId.toString());
		} catch (SQLiteException e) {
			Log.d("Sqlite", e.getMessage());
		}

		if (questions.isEmpty()) {
			hideAll();
			noQuestionText.setVisibility(View.VISIBLE);

		}

		for (Questions i : questions) {

			id = i.getQuestion_id();
			questionTitle = i.getQuestion_title();
			option1 = i.getOption_1();
			option2 = i.getOption_2();
			option3 = i.getOption_3();
			option4 = i.getOption_4();
			right_answer = i.getCorrect_answer();

			Log.d("Question", questionTitle);
			Log.d("Question", option1);
			Log.d("Question", option2);
			Log.d("Question", option3);
			Log.d("Question", option4);
			Log.d("Answer", right_answer);
		}

		question.setText(questionTitle);
		String questionNumberString = questionNumber.toString();

		numberHeader.setText(questionNumberString + "/"
				+ lengthQuestions.toString());

		// randomizes the option
		randomOptions();

		// show tips

		//showTips(answer1, "First option", "Tap to know if you are correct?");
		// showTips(nextBtn, "Next Button", "Tap for next question");
		// showTips(skipTxt, "Skip", "If you want to skip this set tap here");

		answer1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				answer1.getId();

				if (option1.toString().equals(right_answer)) {
					answer1.setBackgroundResource(R.drawable.answer_style_right);
					// score++;
					correctScore++;
					answer1.setClickable(false);
					mainActivityLayout.postDelayed(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							nextQuestion();
						}
					}, TIME_QUESTION);

				} else {
					answer1.setBackgroundResource(R.drawable.answer_style_wrong);
					tryScore++;
				}

			}
		});

		answer2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (option2.equals(right_answer)) {
					answer2.setBackgroundResource(R.drawable.answer_style_right);
					// score++;
					correctScore++;
					answer2.setClickable(false);
					mainActivityLayout.postDelayed(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							nextQuestion();
						}
					}, TIME_QUESTION);

				} else {
					answer2.setBackgroundResource(R.drawable.answer_style_wrong);
					tryScore++;
				}

			}
		});

		answer3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (option3.toString().equals(right_answer)) {
					answer3.setBackgroundResource(R.drawable.answer_style_right);
					// score++;
					correctScore++;
					answer3.setClickable(false);
					mainActivityLayout.postDelayed(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							nextQuestion();
						}
					}, TIME_QUESTION);

				} else {
					answer3.setBackgroundResource(R.drawable.answer_style_wrong);
					tryScore++;
				}

			}
		});

		answer4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (option4.toString().equals(right_answer)) {
					answer4.setBackgroundResource(R.drawable.answer_style_right);
					// score++;
					correctScore++;
					answer4.setClickable(false);
					mainActivityLayout.postDelayed(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							nextQuestion();
						}
					}, TIME_QUESTION);

				} else {
					answer4.setBackgroundResource(R.drawable.answer_style_wrong);
					tryScore++;
				}

			}
		});

		nextBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// questions = db.getAllQuestionsByCategory(1,id+=1);
				nextQuestion();
				// showTips(previousBtn, "Previous Button",
				// "Tap to show previous question");
			}

		});

		previousBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// questions = db.getAllQuestionsByCategory(1,id+=1);
				Log.d("This is question id", initialId.toString());
				nextBtn.setVisibility(View.VISIBLE);
				// make btn clickable
				makeBtnClickable();
				lastId = db.getLastId((Integer.parseInt(categoryIdIntent) + 1),
						Integer.parseInt(setIdIntent) + 1);
				Log.d("Last question id", lastId.toString());

				questions = db.getQuestionsByQuestionId(
						Integer.parseInt(categoryIdIntent) + 1,
						Integer.parseInt(setIdIntent) + 1, initialId--);

				Log.d("Previous intial", initialId.toString());

				for (Questions i : questions) {

					// id = i.getQuestion_id();
					// setId = i.getSet_id();
					questionTitle = i.getQuestion_title();
					option1 = i.getOption_1();
					option2 = i.getOption_2();
					option3 = i.getOption_3();
					option4 = i.getOption_4();
					right_answer = i.getCorrect_answer();

					Log.d("logg", questionTitle);
					Log.d("right", right_answer);

					question.setText(Html.fromHtml(questionTitle));
					answer1.setText(Html.fromHtml(option1));
					answer2.setText(Html.fromHtml(option2));
					answer3.setText(Html.fromHtml(option3));
					answer4.setText(Html.fromHtml(option4));

					questionNumber--;
					String questionNumberString = questionNumber.toString();

					numberHeader.setText(questionNumberString + "/"
							+ lengthQuestions.toString());

					reset();
					hidePreviousBtn();

				}

			}

		});

		skipTxt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				AlertDialog aDialog = showDialog();
				aDialog.show();
			}
		});

	}

	private void showTips(View view, String title, String description) {
		ShowTipsView showtips = new ShowTipsBuilder(this).setTarget(view)
				.setTitle(title).setDescription(description).setDelay(1000)
				.build();

		showtips.show(this);
	}

	private void previousQuestion(){
		// TODO Auto-generated method stub
		// questions = db.getAllQuestionsByCategory(1,id+=1);
		Log.d("This is question id", initialId.toString());
		nextBtn.setVisibility(View.VISIBLE);
		// make btn clickable
		makeBtnClickable();
		lastId = db.getLastId((Integer.parseInt(categoryIdIntent) + 1),
				Integer.parseInt(setIdIntent) + 1);
		Log.d("Last question id", lastId.toString());

		questions = db.getQuestionsByQuestionId(
				Integer.parseInt(categoryIdIntent) + 1,
				Integer.parseInt(setIdIntent) + 1, initialId--);

		Log.d("Previous intial", initialId.toString());

		for (Questions i : questions) {

			// id = i.getQuestion_id();
			// setId = i.getSet_id();
			questionTitle = i.getQuestion_title();
			option1 = i.getOption_1();
			option2 = i.getOption_2();
			option3 = i.getOption_3();
			option4 = i.getOption_4();
			right_answer = i.getCorrect_answer();

			Log.d("logg", questionTitle);
			Log.d("right", right_answer);

			question.setText(Html.fromHtml(questionTitle));
			answer1.setText(Html.fromHtml(option1));
			answer2.setText(Html.fromHtml(option2));
			answer3.setText(Html.fromHtml(option3));
			answer4.setText(Html.fromHtml(option4));

			questionNumber--;
			String questionNumberString = questionNumber.toString();

			numberHeader.setText(questionNumberString + "/"
					+ lengthQuestions.toString());

			reset();
			hidePreviousBtn();
		}
	}

	private void nextQuestion() {
		Log.d("This is set id", setIdIntent.toString());
		makeBtnClickable();
		// shows previous btn
		previousBtn.setVisibility(View.VISIBLE);

		// randomize
		randomOptions();

		// checks score
		if (tryScore < correctScore) {
			++score;
		}
		// scoreTextInt.setText(score.toString());

		tryScore = 0;
		correctScore = 0;

		lastId = db.getLastId((Integer.parseInt(categoryIdIntent) + 1),
				Integer.parseInt(setIdIntent) + 1);
		Log.d("Last question id", lastId.toString());

		questions = db.getQuestionsByQuestionId(
				Integer.parseInt(categoryIdIntent) + 1,
				Integer.parseInt(setIdIntent) + 1, initialId++);

		Log.d("Next initial", initialId.toString());

		// Log.d("id", id.toString());
		Integer lengthQuestions2 = questions.size();
		Log.d("number of question", lengthQuestions2.toString());

		for (Questions i : questions) {

			questionTitle = i.getQuestion_title();
			option1 = i.getOption_1();
			option2 = i.getOption_2();
			option3 = i.getOption_3();
			option4 = i.getOption_4();
			right_answer = i.getCorrect_answer();

			Log.d("Question", questionTitle);
			Log.d("Question", option1);
			Log.d("Question", option2);
			Log.d("Question", option3);
			Log.d("Question", option4);
			Log.d("Answer", right_answer);

			question.setText(Html.fromHtml(questionTitle));
			answer1.setText(Html.fromHtml(option1));
			answer2.setText(Html.fromHtml(option2));
			answer3.setText(Html.fromHtml(option3));
			answer4.setText(Html.fromHtml(option4));
			questionNumber++;
			String questionNumberString = questionNumber.toString();
			Log.d("questi" + "on no.", questionNumberString);
			if (questionNumber > lengthQuestions) {
				questionNumber = lengthQuestions;
				questionNumberString = questionNumber.toString();
			}

			numberHeader.setText(questionNumberString + " /"
					+ lengthQuestions.toString());

			showPreviousBtn();
			reset();

			if (isQuestionOver()) {

				AlertDialog alertDialog = showDialog();
				alertDialog.show();
				hideNextBtn();
			}
		}

	}

	private void makeBtnClickable() {
		answer1.setClickable(true);
		answer2.setClickable(true);
		answer3.setClickable(true);
		answer4.setClickable(true);
	}

	private void randomOptions() {
		int min = 1;
		int max = 4;

		Random r = new Random();
		int nextnumber = r.nextInt(max - min + 1) + min;

		question.setText(questionTitle);
		if (nextnumber == 1) {
			answer1.setText(Html.fromHtml(option1));
			answer2.setText(Html.fromHtml(option2));
			answer3.setText(Html.fromHtml(option3));
			answer4.setText(Html.fromHtml(option4));
		}

		else if (nextnumber == 2) {
			answer1.setText(Html.fromHtml(option1));
			answer2.setText(Html.fromHtml(option2));
			answer3.setText(Html.fromHtml(option3));
			answer4.setText(Html.fromHtml(option4));
		}

		else if (nextnumber == 3) {
			answer1.setText(Html.fromHtml(option1));
			answer2.setText(Html.fromHtml(option2));
			answer3.setText(Html.fromHtml(option3));
			answer4.setText(Html.fromHtml(option4));
		}

		else {
			answer1.setText(Html.fromHtml(option1));
			answer2.setText(Html.fromHtml(option2));
			answer3.setText(Html.fromHtml(option3));
			answer4.setText(Html.fromHtml(option4));
		}
	}

	public boolean isQuestionOver() {
		boolean questionIsOver = false;
		if (initialId > lastId) {
			questionIsOver = true;
		}
		return questionIsOver;
	}

	private void showBtns() {
		nextBtn.setVisibility(View.VISIBLE);
		previousBtn.setVisibility(View.VISIBLE);
	}

	private void hideBtns() {
		nextBtn.setVisibility(View.GONE);
		previousBtn.setVisibility(View.GONE);
	}

	private void intialize() {
		question = (TextView) findViewById(R.id.questionTitle);
		answer1 = (TextView) findViewById(R.id.answer1);
		answer2 = (TextView) findViewById(R.id.answer2);
		answer3 = (TextView) findViewById(R.id.answer3);
		answer4 = (TextView) findViewById(R.id.answer4);

		numberHeader = (TextView) findViewById(R.id.questionHeader);
		categoryHeader = (TextView) findViewById(R.id.categoryHeader);
		nextBtn = (ImageButton) findViewById(R.id.btnNext);
		previousBtn = (ImageButton) findViewById(R.id.btnPrevious);

		mainActivityLayout = (RelativeLayout) findViewById(R.id.mainActivityLayout);
		headerLayout = (LinearLayout) findViewById(R.id.headerMain);

		setHeader = (TextView) findViewById(R.id.setIdHeader);
		// noQuestionText = (TextView) findViewById(R.id.noQuestionTxt);
		skipTxt = (TextView) findViewById(R.id.skipTxtOpt);
		// scoreTextInt = (TextView) findViewById(R.id.scoreTextInt);

		// layout for score
		scoreTxt = (TextView) findViewById(R.id.scoreTxt);
		scoreInt = (TextView) findViewById(R.id.scoreInt);
		scoreLayout = (RelativeLayout) findViewById(R.id.scoreLayout);

		scoreLayout.setVisibility(View.GONE);
		previousBtn.setVisibility(View.INVISIBLE);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/Roboto-Medium.ttf");

		question.setTypeface(tf);
		scoreTxt.setTypeface(tf);
		Typeface tf2 = Typeface.createFromAsset(getAssets(),
				"fonts/Roboto-Thin.ttf");
		scoreInt.setTypeface(tf2);

	}

	public void reset() {
		answer1.setBackgroundResource(R.drawable.grid_style);
		answer2.setBackgroundResource(R.drawable.grid_style);
		answer3.setBackgroundResource(R.drawable.grid_style);
		answer4.setBackgroundResource(R.drawable.grid_style);

	}

	public void hideAll() {
		question.setVisibility(View.GONE);
		answer1.setVisibility(View.GONE);
		answer2.setVisibility(View.GONE);
		answer3.setVisibility(View.GONE);
		answer4.setVisibility(View.GONE);
		nextBtn.setVisibility(View.GONE);
		previousBtn.setVisibility(View.GONE);
		headerLayout.setVisibility(View.GONE);

	}

	public void hidePreviousBtn() {
		if (questionNumber <= 1) {
			previousBtn.setVisibility(View.INVISIBLE);
		}
	}

	public void showPreviousBtn() {
		if (questionNumber > 1) {
			previousBtn.setVisibility(View.VISIBLE);
		}
	}

	public void hideNextBtn() {
		if (initialId > lastId) 
		{
			nextBtn.setVisibility(View.INVISIBLE);
		}
	}

	public void showAll() {
		question.setVisibility(View.VISIBLE);
		answer1.setVisibility(View.VISIBLE);
		answer2.setVisibility(View.VISIBLE);
		answer3.setVisibility(View.VISIBLE);
		answer4.setVisibility(View.VISIBLE);
		previousBtn.setVisibility(View.VISIBLE);
		nextBtn.setVisibility(View.VISIBLE);
		headerLayout.setVisibility(View.VISIBLE);
	}

	private AlertDialog showDialog() {
		AlertDialog alertDialogBuilder = new AlertDialog.Builder(
				MainActivity.this)

				.setTitle("Finish the test?")
				.setMessage(
						"Are you sure you want to finish the test? Click 'Yes' to finish test or 'No' to get back to test")
				.setCancelable(false)
				.setNegativeButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								/*
								 * hideAll();
								 * scoreLayout.setVisibility(View.VISIBLE);
								 * scoreInt.setText(score.toString());
								 */
								finish();
								Intent i = new Intent(MainActivity.this,
										ScoreActivity.class);
								i.putExtra("score", score.toString());
								i.putExtra("categoryId", categoryIdIntent);

								startActivity(i);
								overridePendingTransition(R.anim.slide_in_left,
										R.anim.slide_out_left);
							}
						})
				.setPositiveButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				})

				.create();

		return alertDialogBuilder;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		//menu.findItem(R.id.action_previous).setEnabled(questionNumber > 1);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		/*switch (item.getItemId()) {
		case R.id.action_previous:
			previousQuestion();
		case R.id.action_next:
			nextQuestion();

			break;
		}*/
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {

		AlertDialog alertDialog = showDialog();
		alertDialog.show();
		// back_pressed = System.currentTimeMillis();
	}
}
