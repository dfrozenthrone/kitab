package com.pagoda.kitab;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class ScoreActivity extends Activity implements AnimationListener {

	String score;
	TextView scoreTxt, scoreTitle,nextSetTxt;
	Animation animSideDown, animFadein;
	String categoryIdIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.score_layout);
		ActionBar ab = getActionBar();
		ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006600")));
		ab.hide();
		ab.setHomeButtonEnabled(true);
		
		categoryIdIntent= getIntent().getStringExtra("categoryId");
		score = getIntent().getStringExtra("score");

		scoreTxt = (TextView) findViewById(R.id.scoreInt);
		nextSetTxt = (TextView) findViewById(R.id.nextSetScore);
		
		Typeface tf2 = Typeface.createFromAsset(getAssets(),
				"fonts/Roboto-Thin.ttf");
		scoreTxt.setTypeface(tf2);
		nextSetTxt.setTypeface(tf2);

		animSideDown = AnimationUtils.loadAnimation(getApplicationContext(),
				com.pagoda.kitab.R.anim.slide_down_score);
		animSideDown.setAnimationListener(this);

		scoreTxt.setText(score);
		scoreTxt.setAnimation(animSideDown);
		
		
		nextSetTxt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent(ScoreActivity.this,
						SetListActivity.class);
				i.putExtra("categoryId", categoryIdIntent);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_left);
				finish();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub
		if (animation == animSideDown) {
		}

		if (animation == animFadein) {

		}
	}

	@Override
	public void onAnimationRepeat(Animation arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationStart(Animation arg0) {
		// TODO Auto-generated method stub

	}

}
