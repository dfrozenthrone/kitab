package com.pagoda.kitab;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FeedBackActivity extends Activity {

	EditText nameTxt, emailTxt, detailTxt, contactTxt;
	Button feedbackBtn;
	TextView feedbackTitle;
	ProgressDialog pDialog;
	String name, email, feedback, contact;
	com.pagoda.kitab.utils.JSONParser jsonParser = new com.pagoda.kitab.utils.JSONParser();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback_layout);
		ActionBar ab = getActionBar();
		ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006600")));
		ab.hide();
		ab.setHomeButtonEnabled(true);

		initialize();

		feedbackBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (haveNetworkConnection()) {
					if (nameTxt.getText().toString().trim().equals("")
							|| detailTxt.getText().toString().trim().equals("")
							|| contactTxt.getText().toString().trim()
									.equals("")
							|| emailTxt.getText().toString().trim().equals("")) {
						Toast.makeText(getBaseContext(), " Plz fill out all form",
								3000).show();
					} else {
						new PostFeedback().execute();
					}

				} else {
					Toast.makeText(getApplicationContext(),
							"No internet connection", 3000).show();
				}
			}
		});

	}

	private void initialize() {
		nameTxt = (EditText) findViewById(R.id.nameTxt);
		emailTxt = (EditText) findViewById(R.id.emailTxt);
		detailTxt = (EditText) findViewById(R.id.feedbackTxt);
		contactTxt = (EditText) findViewById(R.id.contactTxt);

		feedbackBtn = (Button) findViewById(R.id.feedbackBtn);
		feedbackTitle = (TextView) findViewById(R.id.feedbackTitle);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/Roboto-Thin.ttf");
		feedbackTitle.setTypeface(tf);
	}

	private boolean haveNetworkConnection() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

	/**
	 * Background Async Task to Create new product
	 * */
	class PostFeedback extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(FeedBackActivity.this);
			pDialog.setMessage("Sending your feedback..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);

			pDialog.show();
		}

		/**
		 * Creating product
		 * */
		protected String doInBackground(String... args) {
			name = nameTxt.getText().toString();
			email = emailTxt.getText().toString();
			feedback = detailTxt.getText().toString();
			contact = contactTxt.getText().toString();

			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("name", name));
			params.add(new BasicNameValuePair("email", email));
			params.add(new BasicNameValuePair("feedback", feedback));
			params.add(new BasicNameValuePair("contact", contact));

			// getting JSON Object
			// Note that create product url accepts POST method

			Log.d("name", name);
			Log.d("email", email);
			Log.d("feedback", feedback);
			Log.d("name", name);
			JSONObject json = jsonParser.makeHttpRequest(
					AppConstants.URL_FEEDBACK, "POST", params);

			// check for success tag
			try {
				Integer success = json.getInt(AppConstants.TAG_SUCCESS);

				Log.d("succs", success.toString());

				if (success == 1) {
					// successfully created product

					Intent i = new Intent(FeedBackActivity.this,
							CategoryActivity.class);

					startActivity(i);
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_left);
					finish();

					Log.d("Success", "Sucess");
				} else {
					// failed to create product
					Toast.makeText(getApplicationContext(),
							"Sorry!! Error occured", 3000).show();

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once done
			pDialog.dismiss();
			Toast.makeText(getApplicationContext(), "Thanks for your feedback", 3000).show();

		}

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		return super.onContextItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

}
